var searchData=
[
  ['major',['major',['../interface_v_m_beacon.html#a19f4239923bc4c472f5189b67cac7898',1,'VMBeacon']]],
  ['maxinstances',['maxInstances',['../interface_v_m_loyalty_card.html#a0b98d021e476d9ba2caad6d58acb8bce',1,'VMLoyaltyCard']]],
  ['maxpointsperday',['maxPointsPerDay',['../interface_v_m_loyalty_card.html#a2ff95118e5d9ae17ba515f4980190c88',1,'VMLoyaltyCard']]],
  ['maxpointsrequestsperday',['maxPointsRequestsPerDay',['../interface_v_m_loyalty_card.html#a1f642a8ee5724d899a8642f52321aed6',1,'VMLoyaltyCard']]],
  ['merchantid',['merchantId',['../interface_v_m_ad_search_criteria.html#a824777cadbcb4b1a0e083c67f55ee974',1,'VMAdSearchCriteria::merchantId()'],['../interface_v_m_base_content.html#a762c9a12c6aca721cec417ca16a246e7',1,'VMBaseContent::merchantId()'],['../interface_v_m_external_merchant.html#a3a4c7d96ac32e4c0bc2b0a10fe2eddb8',1,'VMExternalMerchant::merchantId()'],['../interface_v_m_merchant.html#a80537cf30fd69a09b0ff789ee2405738',1,'VMMerchant::merchantId()'],['../interface_v_m_redeemed_offer.html#aa62df0f5c963dfa335d01d07177cac8b',1,'VMRedeemedOffer::merchantId()'],['../interface_v_m_venue.html#a354539f8cd01eb79b9f40b7f642e5fb2',1,'VMVenue::merchantId()'],['../interface_v_m_venue_search_criteria.html#afa8fa4f8e753ac19e76f044087db5c21',1,'VMVenueSearchCriteria::merchantId()'],['../interface_v_m_loyalty_card_search_criteria.html#aee188483f3b21230d6c390c63d869759',1,'VMLoyaltyCardSearchCriteria::merchantID()'],['../interface_v_m_offer_search_criteria.html#a0d3c2f9d044f4c8f47b59513e85719a9',1,'VMOfferSearchCriteria::merchantID()']]],
  ['merchants',['merchants',['../interface_v_m_favourite_content.html#a3f3cd52d549f4e10a89a3ef376f82776',1,'VMFavouriteContent']]],
  ['minor',['minor',['../interface_v_m_beacon.html#ade0e1fa00be25f0ac083c94b5fcb36e6',1,'VMBeacon']]],
  ['mobilenumber',['mobileNumber',['../interface_v_m_consumer.html#aa2d5052e5b2ac14f1832510e5805cf00',1,'VMConsumer']]],
  ['mobilephonenumber',['mobilePhoneNumber',['../interface_v_m_sign_up_info.html#ac6c0cf4273bf56cf7453f94e834a2c0b',1,'VMSignUpInfo']]]
];
