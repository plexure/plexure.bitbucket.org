var searchData=
[
  ['favouritecontentcreatewithcontentid_3afavouritecontenttype_3acompletionblock_3a',['favouriteContentCreateWithContentId:favouriteContentType:completionBlock:',['../interface_v_m_consumer_manager.html#a24336b2911768c62ae97a20f52a2b5cd',1,'VMConsumerManager']]],
  ['favouritecontentdeletewithcontentid_3afavouritecontenttype_3acompletionblock_3a',['favouriteContentDeleteWithContentId:favouriteContentType:completionBlock:',['../interface_v_m_consumer_manager.html#acafbd43d9e4949214fdb8b0c1005d1c0',1,'VMConsumerManager']]],
  ['favouritecontentlistwithcompletionblock_3a',['favouriteContentListWithCompletionBlock:',['../interface_v_m_consumer_manager.html#aaf42d46f56b7d058dfe7d1213374d080',1,'VMConsumerManager']]],
  ['features',['features',['../interface_v_m_venue.html#aa9261f77a025710c7363d820f89c43d6',1,'VMVenue']]],
  ['feedbackwithrating_3afeedbacktext_3auniqueidentifier_3aextendeddata_3acompletionblock_3a',['feedbackWithRating:feedbackText:uniqueIdentifier:extendedData:completionBlock:',['../interface_v_m_consumer_manager.html#a2fec0fc24b9eca8f3d72ec83243bca28',1,'VMConsumerManager']]],
  ['fields',['fields',['../interface_v_m_offer_search_criteria.html#a4e3818e4255374cd0a3975593d6d2f9b',1,'VMOfferSearchCriteria']]],
  ['fillmultiplecards',['fillMultipleCards',['../interface_v_m_add_points_info.html#aa13a68a7d8c2ae7e144fadd137452e6e',1,'VMAddPointsInfo']]],
  ['firstname',['firstName',['../interface_v_m_consumer.html#a0b2d5132e9dffce1533653d3eb00b83d',1,'VMConsumer::firstName()'],['../interface_v_m_sign_up_info.html#a5aa0b5b337b9247cdf192e2a0a64721e',1,'VMSignUpInfo::firstName()'],['../interface_v_m_sign_up_params.html#ad7659e84981c2c403e975f608712d4d4',1,'VMSignUpParams::firstName()']]],
  ['fullname',['fullName',['../interface_v_m_consumer.html#a8f2e5da61c7b20eeacb88c6c45bd2093',1,'VMConsumer::fullName()'],['../interface_v_m_sign_up_info.html#a7281cce6648d6383a0b618ea5113504e',1,'VMSignUpInfo::fullName()'],['../interface_v_m_sign_up_params.html#a61c5008ee2e73f99989bea83515540a7',1,'VMSignUpParams::fullName()']]]
];
