var searchData=
[
  ['id',['ID',['../struct_offer_field_struct.html#a18a40dab21b1f7c9e8ba5cca4e5023a1',1,'OfferFieldStruct']]],
  ['image',['Image',['../struct_offer_field_struct.html#ada9cd151f07d814c5ae5cc7520d40841',1,'OfferFieldStruct']]],
  ['isactive',['IsActive',['../struct_offer_field_struct.html#ae1c139147c49d0f5ccb056b7f9524d57',1,'OfferFieldStruct']]],
  ['isavailableallstores',['IsAvailableAllStores',['../struct_offer_field_struct.html#a5f617ba5ca17ce818a5b3a048d0e75d1',1,'OfferFieldStruct']]],
  ['isgift',['IsGift',['../struct_offer_field_struct.html#a8323923587b316ce4d8622d75baf213f',1,'OfferFieldStruct']]],
  ['isgiftable',['IsGiftable',['../struct_offer_field_struct.html#a9b5d776101a0c42fa97a7a8e050fd816',1,'OfferFieldStruct']]],
  ['ismerchantfavourite',['IsMerchantFavourite',['../struct_offer_field_struct.html#a9640824922786dab3d91bf6ac198943b',1,'OfferFieldStruct']]],
  ['ispremiumplacement',['IsPremiumPlacement',['../struct_offer_field_struct.html#a8bcef8178e8c25d7d95940c403240012',1,'OfferFieldStruct']]],
  ['isrespawning',['IsRespawning',['../struct_offer_field_struct.html#a125db15205ef6c0350d9522124729bef',1,'OfferFieldStruct']]],
  ['isreward',['IsReward',['../struct_offer_field_struct.html#a895f42154039a275712211ada810c549',1,'OfferFieldStruct']]]
];
