var searchData=
[
  ['radius',['radius',['../interface_v_m_geo_fence.html#a3672e44f46f99e28c0fa3ff2d679e7db',1,'VMGeoFence']]],
  ['rankedsearch',['rankedSearch',['../interface_v_m_offer_search_criteria.html#ae65054acdccafdcd27f28bf1c2674eaa',1,'VMOfferSearchCriteria']]],
  ['redeemedofferid',['redeemedOfferId',['../interface_v_m_loyalty_card_instance.html#a090795f29489217028762ef2cdd4e515',1,'VMLoyaltyCardInstance']]],
  ['redemptioncount',['redemptionCount',['../interface_v_m_offer.html#ae556948747419783b8fbc1ce2f11605d',1,'VMOffer']]],
  ['redemptiondate',['redemptionDate',['../interface_v_m_redeemed_offer.html#a2e368f0521000c992aece49883298eb5',1,'VMRedeemedOffer']]],
  ['redemptionexpirydate',['redemptionExpiryDate',['../interface_v_m_redeemed_offer.html#ae6acd37c5bc9e68616a144f57ed91752',1,'VMRedeemedOffer']]],
  ['redemptionimagename',['redemptionImageName',['../interface_v_m_redeemed_offer.html#acbd4682878a533de2bc50b36bdb1bb1c',1,'VMRedeemedOffer']]],
  ['redemptiontext',['redemptionText',['../interface_v_m_redeemed_offer.html#ae90f17b37a61b8ef87eb36c3ae1c5bec',1,'VMRedeemedOffer']]],
  ['redemptiontype',['redemptionType',['../interface_v_m_offer.html#ab3ed9359af484f2df9877a1bf3bdd9b7',1,'VMOffer::redemptionType()'],['../interface_v_m_redeemed_offer.html#a430eae84e1fe0e26714a97de0672a022',1,'VMRedeemedOffer::redemptionType()']]],
  ['regionid',['regionId',['../interface_v_m_venue.html#a9607ee40863485d7a50aecf922c061a7',1,'VMVenue::regionId()'],['../interface_v_m_weighted_content_search_criteria.html#a97e6fb78954f092c3ce1c92589f509fe',1,'VMWeightedContentSearchCriteria::regionId()']]],
  ['respawndaysnumber',['respawnDaysNumber',['../interface_v_m_offer.html#ad2a9586271c770fb9802a4237b433616',1,'VMOffer']]],
  ['reward',['reward',['../interface_v_m_redeemed_offer.html#a1a2dabff31bd597ab4253d5f49a79e74',1,'VMRedeemedOffer']]]
];
