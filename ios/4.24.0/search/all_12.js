var searchData=
[
  ['uiapplication_28vmruntime_29',['UIApplication(VMRuntime)',['../category_u_i_application_07_v_m_runtime_08.html',1,'']]],
  ['uiapplication_2bvmruntime_2eh',['UIApplication+VMRuntime.h',['../_u_i_application_09_v_m_runtime_8h.html',1,'']]],
  ['updateconsumer_3acompletionblock_3a',['updateConsumer:completionBlock:',['../interface_v_m_consumer_manager.html#a56089d6bb02fd38b28c9be286a537fe2',1,'VMConsumerManager']]],
  ['updateconsumer_3averificationtoken_3acompletionblock_3a',['updateConsumer:verificationToken:completionBlock:',['../interface_v_m_consumer_manager.html#ab5842c3e917339f0a529f7ab876babdb',1,'VMConsumerManager']]],
  ['updateconsumerwithconsumer_3acompletionblock_3a',['updateConsumerWithConsumer:completionBlock:',['../interface_v_m_consumer_manager.html#a864220469fb279b49314f349e3dd4b5c',1,'VMConsumerManager']]],
  ['updateconsumerwithconsumer_3averificationtoken_3acompletionblock_3a',['updateConsumerWithConsumer:verificationToken:completionBlock:',['../interface_v_m_consumer_manager.html#af7d3eab0f96790d7063eacd130db1133',1,'VMConsumerManager']]],
  ['updateconsumerwithusername_3afirstname_3alastname_3afullname_3agender_3adateofbirth_3acity_3aemailaddress_3aextendeddata_3acompletionblock_3a',['updateConsumerWithUserName:firstName:lastName:fullName:gender:dateOfBirth:city:emailAddress:extendedData:completionBlock:',['../interface_v_m_consumer_manager.html#a0aeb72dab5682d8c050cc351db63e893',1,'VMConsumerManager']]],
  ['updateconsumerwithusername_3afirstname_3alastname_3afullname_3agender_3adateofbirth_3acity_3aemailaddress_3aextendeddata_3averificationtoken_3acompletionblock_3a',['updateConsumerWithUserName:firstName:lastName:fullName:gender:dateOfBirth:city:emailAddress:extendedData:verificationToken:completionBlock:',['../interface_v_m_consumer_manager.html#a8ab581a7597f79d621374862128cc8c4',1,'VMConsumerManager']]],
  ['updategeofenceswithresultblock_3a',['updateGeofencesWithResultBlock:',['../interface_v_m_geofences_manager.html#a404fb86d96510e98655dbdb5bc8e1901',1,'VMGeofencesManager']]],
  ['updatetagswithtagstoadd_3atagstoremove_3acompletionblock_3a',['updateTagsWithTagsToAdd:tagsToRemove:completionBlock:',['../interface_v_m_consumer_manager.html#a5ca561ef0200695629519456b56f2e6e',1,'VMConsumerManager']]],
  ['updatetagwithtagvalueaddreferencecodes_3atagvalueremovereferencecodes_3acompletionblock_3a',['updateTagWithTagValueAddReferenceCodes:tagValueRemoveReferenceCodes:completionBlock:',['../interface_v_m_consumer_manager.html#a02fe34a064efa07df86cee96ae642b0c',1,'VMConsumerManager']]],
  ['username',['userName',['../interface_v_m_consumer.html#ac23e4330eab4f0791471d2a4e84b3622',1,'VMConsumer::userName()'],['../interface_v_m_login_info.html#aeac48d2b50a0c91e9808dd8647d790bc',1,'VMLoginInfo::username()'],['../interface_v_m_sign_up_info.html#a172ff9032a02b3ed5b18ab459e31a8ff',1,'VMSignUpInfo::username()'],['../interface_v_m_sign_up_params.html#ad3fb26956932fa1b7ae6d92eae27157b',1,'VMSignUpParams::username()']]],
  ['uuid',['uuid',['../interface_v_m_beacon.html#a0e672a55d1ddc9d20a1d6b891abffe8c',1,'VMBeacon']]]
];
