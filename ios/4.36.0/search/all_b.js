var searchData=
[
  ['major',['major',['../interface_v_m_beacon.html#a65bc29f0652b023f67a82c15719b8f5e',1,'VMBeacon']]],
  ['maxinstances',['maxInstances',['../interface_v_m_loyalty_card.html#a0b98d021e476d9ba2caad6d58acb8bce',1,'VMLoyaltyCard']]],
  ['maxpointsperday',['maxPointsPerDay',['../interface_v_m_loyalty_card.html#a2ff95118e5d9ae17ba515f4980190c88',1,'VMLoyaltyCard']]],
  ['maxpointsrequestsperday',['maxPointsRequestsPerDay',['../interface_v_m_loyalty_card.html#a1f642a8ee5724d899a8642f52321aed6',1,'VMLoyaltyCard']]],
  ['merchantid',['merchantId',['../interface_v_m_ad_search_criteria.html#a824777cadbcb4b1a0e083c67f55ee974',1,'VMAdSearchCriteria::merchantId()'],['../interface_v_m_base_content.html#a762c9a12c6aca721cec417ca16a246e7',1,'VMBaseContent::merchantId()'],['../interface_v_m_external_merchant.html#a3a4c7d96ac32e4c0bc2b0a10fe2eddb8',1,'VMExternalMerchant::merchantId()'],['../interface_v_m_merchant.html#a80537cf30fd69a09b0ff789ee2405738',1,'VMMerchant::merchantId()'],['../interface_v_m_redeemed_offer.html#aa62df0f5c963dfa335d01d07177cac8b',1,'VMRedeemedOffer::merchantId()'],['../interface_v_m_venue.html#a354539f8cd01eb79b9f40b7f642e5fb2',1,'VMVenue::merchantId()'],['../interface_v_m_venue_search_criteria.html#afa8fa4f8e753ac19e76f044087db5c21',1,'VMVenueSearchCriteria::merchantId()'],['../struct_offer_field_struct.html#aac5e9b3e21e9307dd1315a0879dd2822',1,'OfferFieldStruct::MerchantId()'],['../interface_v_m_loyalty_card_search_criteria.html#aee188483f3b21230d6c390c63d869759',1,'VMLoyaltyCardSearchCriteria::merchantID()'],['../interface_v_m_offer_search_criteria.html#a0d3c2f9d044f4c8f47b59513e85719a9',1,'VMOfferSearchCriteria::merchantID()']]],
  ['merchantlistwithcategoryid_3aoffset_3alimit_3acompletionblock_3a',['merchantListWithCategoryId:offset:limit:completionBlock:',['../interface_v_m_merchants_manager.html#a63fad6f62bdfb548b47fca3d8726dfc3',1,'VMMerchantsManager']]],
  ['merchants',['merchants',['../interface_v_m_favourite_content.html#a3f3cd52d549f4e10a89a3ef376f82776',1,'VMFavouriteContent']]],
  ['merchantsmanager',['merchantsManager',['../interface_v_mob.html#a30be9857a2f6fb7645ca17b1613e1a94',1,'VMob']]],
  ['minor',['minor',['../interface_v_m_beacon.html#ae202eeffb3c4bf8afc2d9d644d46f6d1',1,'VMBeacon']]],
  ['mobilenumber',['mobileNumber',['../interface_v_m_consumer.html#aa2d5052e5b2ac14f1832510e5805cf00',1,'VMConsumer']]],
  ['mobilephonenumber',['mobilePhoneNumber',['../interface_v_m_sign_up_info.html#ac6c0cf4273bf56cf7453f94e834a2c0b',1,'VMSignUpInfo']]]
];
