var searchData=
[
  ['radius',['radius',['../interface_v_m_geo_fence.html#a3672e44f46f99e28c0fa3ff2d679e7db',1,'VMGeoFence']]],
  ['rankedsearch',['rankedSearch',['../interface_v_m_offer_search_criteria.html#ae65054acdccafdcd27f28bf1c2674eaa',1,'VMOfferSearchCriteria']]],
  ['redeemedofferid',['redeemedOfferId',['../interface_v_m_loyalty_card_instance.html#a090795f29489217028762ef2cdd4e515',1,'VMLoyaltyCardInstance']]],
  ['redeemofferwithid_3agiftid_3aofferinstanceid_3acompletionblock_3a',['redeemOfferWithId:giftId:offerInstanceId:completionBlock:',['../interface_v_m_offers_manager.html#a07afc3100ac8f59702d6c2a5d6c824fb',1,'VMOffersManager']]],
  ['redemptioncount',['RedemptionCount',['../struct_offer_field_struct.html#a2829d4908db9384dc313a40d66f6db7d',1,'OfferFieldStruct::RedemptionCount()'],['../interface_v_m_offer.html#ae556948747419783b8fbc1ce2f11605d',1,'VMOffer::redemptionCount()']]],
  ['redemptiondate',['redemptionDate',['../interface_v_m_redeemed_offer.html#a2e368f0521000c992aece49883298eb5',1,'VMRedeemedOffer']]],
  ['redemptionexpirydate',['redemptionExpiryDate',['../interface_v_m_redeemed_offer.html#ae6acd37c5bc9e68616a144f57ed91752',1,'VMRedeemedOffer']]],
  ['redemptionimagename',['redemptionImageName',['../interface_v_m_redeemed_offer.html#acbd4682878a533de2bc50b36bdb1bb1c',1,'VMRedeemedOffer']]],
  ['redemptiontext',['redemptionText',['../interface_v_m_redeemed_offer.html#ae90f17b37a61b8ef87eb36c3ae1c5bec',1,'VMRedeemedOffer']]],
  ['redemptiontype',['redemptionType',['../interface_v_m_offer.html#ab3ed9359af484f2df9877a1bf3bdd9b7',1,'VMOffer::redemptionType()'],['../interface_v_m_redeemed_offer.html#a430eae84e1fe0e26714a97de0672a022',1,'VMRedeemedOffer::redemptionType()'],['../struct_offer_field_struct.html#a132711de59e66324eb7637441f166572',1,'OfferFieldStruct::RedemptionType()']]],
  ['regionid',['regionId',['../interface_v_m_venue.html#a9607ee40863485d7a50aecf922c061a7',1,'VMVenue::regionId()'],['../interface_v_m_weighted_content_search_criteria.html#a97e6fb78954f092c3ce1c92589f509fe',1,'VMWeightedContentSearchCriteria::regionId()']]],
  ['registernotification_3a',['registerNotification:',['../category_u_i_application_07_v_m_runtime_08.html#a2febb17f0c1373557bc0957f4ff5b993',1,'UIApplication(VMRuntime)']]],
  ['removefavouritecontentwithid_3atype_3acompletionblock_3a',['removeFavouriteContentWithId:type:completionBlock:',['../interface_v_m_consumer_manager.html#ac2c240805274e0cbbd6dbdaf56a4391d',1,'VMConsumerManager']]],
  ['removetags_3acompletionblock_3a',['removeTags:completionBlock:',['../interface_v_m_consumer_manager.html#a9c300c6710f555e863ef2f1bca034722',1,'VMConsumerManager']]],
  ['removetagvaluereferencecodes_3acompletionblock_3a',['removeTagValueReferenceCodes:completionBlock:',['../interface_v_m_consumer_manager.html#a352e3c54dedad95fce0e86080e68855e',1,'VMConsumerManager']]],
  ['requesterrorwithcode_3adescription_3a',['requestErrorWithCode:description:',['../interface_v_m_request_error.html#ae15fd06faae55e46d343ec2bdb6886ad',1,'VMRequestError']]],
  ['requestpasswordresetwithusername_3acompletionblock_3a',['requestPasswordResetWithUsername:completionBlock:',['../interface_v_m_authentication_manager.html#a94b93015335d2e6fc05165684e8857ee',1,'VMAuthenticationManager']]],
  ['resetpasswordwithverificationtoken_3anewpassword_3acompletionblock_3a',['resetPasswordWithVerificationToken:newPassword:completionBlock:',['../interface_v_m_authentication_manager.html#a4f91752c0f60a9befba0ed57bc65edbc',1,'VMAuthenticationManager']]],
  ['respawndaysnumber',['respawnDaysNumber',['../interface_v_m_offer.html#ad2a9586271c770fb9802a4237b433616',1,'VMOffer']]],
  ['respawnlimit',['RespawnLimit',['../struct_offer_field_struct.html#a341668bb1729a7a51e57bbc6d2b9315d',1,'OfferFieldStruct::RespawnLimit()'],['../interface_v_m_offer.html#a6def382a992dbdc5df23b7d5ebc78aea',1,'VMOffer::respawnLimit()']]],
  ['respawnsindays',['RespawnsInDays',['../struct_offer_field_struct.html#a1d3428348e059cfd7592475bad87a5dd',1,'OfferFieldStruct']]],
  ['reward',['reward',['../interface_v_m_redeemed_offer.html#a1a2dabff31bd597ab4253d5f49a79e74',1,'VMRedeemedOffer']]]
];
