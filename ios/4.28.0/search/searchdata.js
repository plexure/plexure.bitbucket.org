var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvw",
  1: "cnouv",
  2: "cnuv",
  3: "abcdefghilmnoprsuvw",
  4: "_abcdegiklmoprstvw",
  5: "bv",
  6: "v",
  7: "dv",
  8: "abcdefghiklmnoprstuvw",
  9: "ev",
  10: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Macros",
  10: "Pages"
};

