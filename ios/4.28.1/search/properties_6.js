var searchData=
[
  ['gender',['gender',['../interface_v_m_consumer.html#a5887766f9fe7afd4196a146ceb96cec4',1,'VMConsumer::gender()'],['../interface_v_m_sign_up_info.html#a293fb209c475f4f578b25de2d0d18dc2',1,'VMSignUpInfo::gender()'],['../interface_v_m_sign_up_params.html#ae67ff609484934ea267f75fa951585a2',1,'VMSignUpParams::gender()']]],
  ['geofenceid',['geofenceId',['../interface_v_m_geo_fence.html#ab8b311cbf1f6de27c1392210954a7cb4',1,'VMGeoFence']]],
  ['geotilesize',['geoTileSize',['../interface_v_m_server_configuration.html#abad708e26c46b0e74180059994d26b10',1,'VMServerConfiguration']]],
  ['giftbatchid',['giftBatchID',['../interface_v_m_offer.html#a34944fadd1f597d4fecac60a91df6cb5',1,'VMOffer::giftBatchID()'],['../interface_v_m_redeemed_offer.html#a1262aa45eac067921c28ff483597e44f',1,'VMRedeemedOffer::giftBatchId()']]],
  ['gifteddate',['giftedDate',['../interface_v_m_offer.html#aec865298b8aad49e3ad81d168918fdb5',1,'VMOffer::giftedDate()'],['../interface_v_m_redeemed_offer.html#a3e8acf4bd69c780dd254b282d1ade61a',1,'VMRedeemedOffer::giftedDate()']]],
  ['giftid',['giftID',['../interface_v_m_offer.html#a69836523bab30f48e1933b87bc749cb2',1,'VMOffer::giftID()'],['../interface_v_m_redeemed_offer.html#aa28dd8679736096ac2789de22e794213',1,'VMRedeemedOffer::giftId()']]],
  ['giftmessage',['giftMessage',['../interface_v_m_offer.html#ad40a68d5314a7982c9215f8073a8da34',1,'VMOffer']]],
  ['giftsender',['giftSender',['../interface_v_m_offer.html#afcd7f85f59c8e18ae7294694a6541e05',1,'VMOffer']]]
];
