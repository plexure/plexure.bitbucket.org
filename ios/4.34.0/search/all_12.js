var searchData=
[
  ['uiapplication_28vmruntime_29',['UIApplication(VMRuntime)',['../category_u_i_application_07_v_m_runtime_08.html',1,'']]],
  ['uiapplication_2bvmruntime_2eh',['UIApplication+VMRuntime.h',['../_u_i_application_09_v_m_runtime_8h.html',1,'']]],
  ['updateconsentstatus_3acompletionblock_3a',['updateConsentStatus:completionBlock:',['../interface_v_m_consumer_manager.html#a90db87e21697e724977d64b637aa64c3',1,'VMConsumerManager']]],
  ['updateconsumer_3acompletionblock_3a',['updateConsumer:completionBlock:',['../interface_v_m_consumer_manager.html#a1c9a3636742d945eb6a167df223d919c',1,'VMConsumerManager']]],
  ['updateconsumer_3averificationtoken_3acompletionblock_3a',['updateConsumer:verificationToken:completionBlock:',['../interface_v_m_consumer_manager.html#a7665bd6edfa6d8ddabd989dd6bf48e31',1,'VMConsumerManager']]],
  ['updateconsumerwithconsumer_3acompletionblock_3a',['updateConsumerWithConsumer:completionBlock:',['../interface_v_m_consumer_manager.html#a132c694e79cb03ea7c7005ae654db970',1,'VMConsumerManager']]],
  ['updateconsumerwithconsumer_3averificationtoken_3acompletionblock_3a',['updateConsumerWithConsumer:verificationToken:completionBlock:',['../interface_v_m_consumer_manager.html#a02ec1b893cb4c807d67642a71bf23c3a',1,'VMConsumerManager']]],
  ['updateconsumerwithusername_3afirstname_3alastname_3afullname_3agender_3adateofbirth_3acity_3aemailaddress_3aextendeddata_3acompletionblock_3a',['updateConsumerWithUserName:firstName:lastName:fullName:gender:dateOfBirth:city:emailAddress:extendedData:completionBlock:',['../interface_v_m_consumer_manager.html#ad2e21530f93eca02f5131b9e718f9d78',1,'VMConsumerManager']]],
  ['updateconsumerwithusername_3afirstname_3alastname_3afullname_3agender_3adateofbirth_3acity_3aemailaddress_3aextendeddata_3averificationtoken_3acompletionblock_3a',['updateConsumerWithUserName:firstName:lastName:fullName:gender:dateOfBirth:city:emailAddress:extendedData:verificationToken:completionBlock:',['../interface_v_m_consumer_manager.html#aa58e414138e65c71efd1a68768bdad5e',1,'VMConsumerManager']]],
  ['updatetagswithtagstoadd_3atagstoremove_3acompletionblock_3a',['updateTagsWithTagsToAdd:tagsToRemove:completionBlock:',['../interface_v_m_consumer_manager.html#af3e17c8cbebda82f7c927ea8f3806895',1,'VMConsumerManager']]],
  ['updatetagwithtagvalueaddreferencecodes_3atagvalueremovereferencecodes_3acompletionblock_3a',['updateTagWithTagValueAddReferenceCodes:tagValueRemoveReferenceCodes:completionBlock:',['../interface_v_m_consumer_manager.html#a9c7e7c9d43b50b4ea00a6d2f97aa8758',1,'VMConsumerManager']]],
  ['username',['userName',['../interface_v_m_consumer.html#ac23e4330eab4f0791471d2a4e84b3622',1,'VMConsumer::userName()'],['../interface_v_m_login_info.html#aeac48d2b50a0c91e9808dd8647d790bc',1,'VMLoginInfo::username()'],['../interface_v_m_sign_up_info.html#a172ff9032a02b3ed5b18ab459e31a8ff',1,'VMSignUpInfo::username()'],['../interface_v_m_sign_up_params.html#ad3fb26956932fa1b7ae6d92eae27157b',1,'VMSignUpParams::username()']]],
  ['uuid',['uuid',['../interface_v_m_beacon.html#a0e672a55d1ddc9d20a1d6b891abffe8c',1,'VMBeacon']]]
];
