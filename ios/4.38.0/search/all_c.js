var searchData=
[
  ['name',['name',['../interface_v_m_category.html#aaafff1b0eb20ccd1b5fce843ab1bcb61',1,'VMCategory::name()'],['../interface_v_m_city.html#a35b29053ff6cb684e711a20781911df5',1,'VMCity::name()'],['../interface_v_m_content_tag.html#a0508f2ea99ac04754541ba8bed3808b7',1,'VMContentTag::name()'],['../interface_v_m_external_merchant.html#a2f196859b8f10f1c0d7b3d592269827f',1,'VMExternalMerchant::name()'],['../interface_v_m_merchant.html#a0494b7e0fa5452086ba04b3831c5a53f',1,'VMMerchant::name()'],['../interface_v_m_state.html#a33f5b5455128cbddf8335bea41dcefaf',1,'VMState::name()'],['../interface_v_m_venue.html#a2f62a367952ca63ad28866e5b6355925',1,'VMVenue::name()']]],
  ['note',['note',['../interface_v_m_open_hours.html#adbe466ff035fe5d0a794d38ffb6d8586',1,'VMOpenHours']]],
  ['ns_5fenum',['NS_ENUM',['../_v_m_content_constants_8h.html#a6a173fb7c4352f49fccfd1da28db8ca0',1,'VMContentConstants.h']]],
  ['nsdate_28vmadditions_29',['NSDate(VMAdditions)',['../category_n_s_date_07_v_m_additions_08.html',1,'']]],
  ['nsdate_2bvmadditions_2eh',['NSDate+VMAdditions.h',['../_n_s_date_09_v_m_additions_8h.html',1,'']]],
  ['nsdictionary',['NSDictionary',['../interface_n_s_dictionary.html',1,'']]],
  ['nsdictionary_2bvmadditions_2eh',['NSDictionary+VMAdditions.h',['../_n_s_dictionary_09_v_m_additions_8h.html',1,'']]],
  ['nsdictionary_3c_20nsstring_20_2a_2c_20nsnumber_20_2a_20_3e',['NSDictionary&lt; NSString *, NSNumber * &gt;',['../interface_n_s_dictionary.html',1,'']]],
  ['nsmutabledictionary',['NSMutableDictionary',['../interface_n_s_mutable_dictionary.html',1,'']]],
  ['nsstring_28vmadditions_29',['NSString(VMAdditions)',['../category_n_s_string_07_v_m_additions_08.html',1,'']]],
  ['nsstring_2bvmadditions_2eh',['NSString+VMAdditions.h',['../_n_s_string_09_v_m_additions_8h.html',1,'']]],
  ['numactiveoffers',['numActiveOffers',['../interface_v_m_merchant.html#ad7e1c898d427fac452de323f3c50466b',1,'VMMerchant']]]
];
