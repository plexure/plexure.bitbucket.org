var searchData=
[
  ['redeemofferwithid_3agiftid_3aofferinstanceid_3acompletionblock_3a',['redeemOfferWithId:giftId:offerInstanceId:completionBlock:',['../interface_v_m_offers_manager.html#a07afc3100ac8f59702d6c2a5d6c824fb',1,'VMOffersManager']]],
  ['removefavouritecontentwithid_3atype_3acompletionblock_3a',['removeFavouriteContentWithId:type:completionBlock:',['../interface_v_m_consumer_manager.html#ac2c240805274e0cbbd6dbdaf56a4391d',1,'VMConsumerManager']]],
  ['removetags_3acompletionblock_3a',['removeTags:completionBlock:',['../interface_v_m_consumer_manager.html#a9c300c6710f555e863ef2f1bca034722',1,'VMConsumerManager']]],
  ['removetagvaluereferencecodes_3acompletionblock_3a',['removeTagValueReferenceCodes:completionBlock:',['../interface_v_m_consumer_manager.html#a352e3c54dedad95fce0e86080e68855e',1,'VMConsumerManager']]],
  ['requesterrorwithcode_3adescription_3a',['requestErrorWithCode:description:',['../interface_v_m_request_error.html#ae15fd06faae55e46d343ec2bdb6886ad',1,'VMRequestError']]],
  ['requestpasswordresetwithusername_3acompletionblock_3a',['requestPasswordResetWithUsername:completionBlock:',['../interface_v_m_authentication_manager.html#a94b93015335d2e6fc05165684e8857ee',1,'VMAuthenticationManager']]],
  ['resetpasswordwithverificationtoken_3anewpassword_3acompletionblock_3a',['resetPasswordWithVerificationToken:newPassword:completionBlock:',['../interface_v_m_authentication_manager.html#a4f91752c0f60a9befba0ed57bc65edbc',1,'VMAuthenticationManager']]]
];
