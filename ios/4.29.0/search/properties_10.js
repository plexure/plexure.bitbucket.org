var searchData=
[
  ['signalstrength',['signalStrength',['../interface_v_m_beacon.html#a53204ad91271a7256ff4854793a1641d',1,'VMBeacon']]],
  ['signuptype',['signUpType',['../interface_v_m_sign_up_info.html#a934803de580bf4a3ea557e00452235f5',1,'VMSignUpInfo']]],
  ['socialsource',['socialSource',['../interface_v_m_social_connection_info.html#ac7d1089aae66b67344e0e4c3ffd48138',1,'VMSocialConnectionInfo::socialSource()'],['../interface_v_m_social_login_info.html#a7daee41a104a9e62b69b68d0b6db79f7',1,'VMSocialLoginInfo::socialSource()']]],
  ['sortcriteria',['sortCriteria',['../interface_v_m_offer_search_criteria.html#adbbf180be9bb5321d62d29f115ff1b10',1,'VMOfferSearchCriteria']]],
  ['sortorder',['sortOrder',['../interface_v_m_category.html#a5f50c160b2933fe76e539e3d9a17099e',1,'VMCategory']]],
  ['start',['start',['../interface_v_m_open_hours.html#a391d14016fb0b5491622070df7544147',1,'VMOpenHours']]],
  ['startdate',['startDate',['../interface_v_m_base_content.html#a201513bf31faabf1ce05e7d021c9cf94',1,'VMBaseContent::startDate()'],['../interface_v_m_loyalty_card.html#a49755eca4b40029d0921f93cfe427109',1,'VMLoyaltyCard::startDate()'],['../interface_v_m_redeemed_offer.html#a0c1567fbe4fd7e7e0e4b503f3e54b99c',1,'VMRedeemedOffer::startDate()']]],
  ['stateid',['stateId',['../interface_v_m_city.html#a99cdea4e62bc1956c52457229e201bb1',1,'VMCity::stateId()'],['../interface_v_m_state.html#a1829192877cae71b13c6d7a83811406d',1,'VMState::stateId()']]],
  ['subtitle',['subTitle',['../interface_v_m_loyalty_card.html#ac3162812ae5f405685a809a7b3be68d7',1,'VMLoyaltyCard']]]
];
