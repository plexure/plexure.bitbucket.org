var searchData=
[
  ['name',['name',['../interface_v_m_category.html#aaafff1b0eb20ccd1b5fce843ab1bcb61',1,'VMCategory::name()'],['../interface_v_m_city.html#a35b29053ff6cb684e711a20781911df5',1,'VMCity::name()'],['../interface_v_m_content_tag.html#a0508f2ea99ac04754541ba8bed3808b7',1,'VMContentTag::name()'],['../interface_v_m_external_merchant.html#a2f196859b8f10f1c0d7b3d592269827f',1,'VMExternalMerchant::name()'],['../interface_v_m_merchant.html#a0494b7e0fa5452086ba04b3831c5a53f',1,'VMMerchant::name()'],['../interface_v_m_state.html#a33f5b5455128cbddf8335bea41dcefaf',1,'VMState::name()'],['../interface_v_m_venue.html#a2f62a367952ca63ad28866e5b6355925',1,'VMVenue::name()']]],
  ['note',['note',['../interface_v_m_open_hours.html#adbe466ff035fe5d0a794d38ffb6d8586',1,'VMOpenHours']]],
  ['numactiveoffers',['numActiveOffers',['../interface_v_m_merchant.html#ad7e1c898d427fac452de323f3c50466b',1,'VMMerchant']]]
];
