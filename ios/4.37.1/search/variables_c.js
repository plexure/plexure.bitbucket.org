var searchData=
[
  ['redemptioncodevalidtime',['RedemptionCodeValidTime',['../struct_offer_field_struct.html#af6c9fad283f2a8ef9505f3a126ff49fe',1,'OfferFieldStruct']]],
  ['redemptioncount',['RedemptionCount',['../struct_offer_field_struct.html#a2829d4908db9384dc313a40d66f6db7d',1,'OfferFieldStruct']]],
  ['redemptiontype',['RedemptionType',['../struct_offer_field_struct.html#a132711de59e66324eb7637441f166572',1,'OfferFieldStruct']]],
  ['repeatdaysnumber',['RepeatDaysNumber',['../struct_offer_field_struct.html#a8e27693b7bd57036bd6d32545ce87333',1,'OfferFieldStruct']]],
  ['repeatlimit',['RepeatLimit',['../struct_offer_field_struct.html#a805eb569b108ee41bd21e1868a2718df',1,'OfferFieldStruct']]],
  ['respawnlimit',['RespawnLimit',['../struct_offer_field_struct.html#a341668bb1729a7a51e57bbc6d2b9315d',1,'OfferFieldStruct']]],
  ['respawnsindays',['RespawnsInDays',['../struct_offer_field_struct.html#a1d3428348e059cfd7592475bad87a5dd',1,'OfferFieldStruct']]]
];
