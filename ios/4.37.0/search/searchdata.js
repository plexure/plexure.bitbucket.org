var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvw",
  1: "nov",
  2: "nv",
  3: "abcdefghilmnoprsuvw",
  4: "abcdegiklmoprstvw",
  5: "v",
  6: "v",
  7: "dv",
  8: "abcdefghiklmnoprstuvw",
  9: "v",
  10: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Macros",
  10: "Pages"
};

