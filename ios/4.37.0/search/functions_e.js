var searchData=
[
  ['redeemofferwithid_3agiftid_3aofferinstanceid_3acompletionblock_3a',['redeemOfferWithId:giftId:offerInstanceId:completionBlock:',['../interface_v_m_offers_manager.html#a97d34756a854ab9369835283e67a1b6b',1,'VMOffersManager']]],
  ['removefavouritecontentwithid_3atype_3acompletionblock_3a',['removeFavouriteContentWithId:type:completionBlock:',['../interface_v_m_consumer_manager.html#ad27eaa336b50c8b3363cf46327fc5eda',1,'VMConsumerManager']]],
  ['removetags_3acompletionblock_3a',['removeTags:completionBlock:',['../interface_v_m_consumer_manager.html#a7018addf3b277431409c1c5f8e3c626c',1,'VMConsumerManager']]],
  ['removetagvaluereferencecodes_3acompletionblock_3a',['removeTagValueReferenceCodes:completionBlock:',['../interface_v_m_consumer_manager.html#a119f00c929e59e1a5a56854ad72381e5',1,'VMConsumerManager']]],
  ['requestconsenthistorywithcompletionblock_3a',['requestConsentHistoryWithCompletionBlock:',['../interface_v_m_consumer_manager.html#a74bbb45ef2ee29123a4f0b1ee829a5db',1,'VMConsumerManager']]],
  ['requesterrorwithcode_3adescription_3a',['requestErrorWithCode:description:',['../interface_v_m_request_error.html#a2b9ddd14236c8c55fdf604817da81914',1,'VMRequestError']]],
  ['requestpasswordresetwithusername_3acompletionblock_3a',['requestPasswordResetWithUsername:completionBlock:',['../interface_v_m_authentication_manager.html#a23fba71e304912ebf2916157a6ad7d9c',1,'VMAuthenticationManager']]],
  ['requestpersonalinfowithcompletionblock_3a',['requestPersonalInfoWithCompletionBlock:',['../interface_v_m_consumer_manager.html#ac2d2883a2308b1c1084e490bb62d4a50',1,'VMConsumerManager']]],
  ['resetpasswordwithverificationtoken_3anewpassword_3acompletionblock_3a',['resetPasswordWithVerificationToken:newPassword:completionBlock:',['../interface_v_m_authentication_manager.html#ad820ab445d64c34ccd0c635ca6bd0861',1,'VMAuthenticationManager']]]
];
