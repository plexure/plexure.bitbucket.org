var searchData=
[
  ['favouritecontentcreatewithcontentid_3afavouritecontenttype_3acompletionblock_3a',['favouriteContentCreateWithContentId:favouriteContentType:completionBlock:',['../interface_v_m_consumer_manager.html#a02e213c5af844ee294e7edf2ea4fe869',1,'VMConsumerManager']]],
  ['favouritecontentdeletewithcontentid_3afavouritecontenttype_3acompletionblock_3a',['favouriteContentDeleteWithContentId:favouriteContentType:completionBlock:',['../interface_v_m_consumer_manager.html#ad5a3a90629f4818745aceec183c490ed',1,'VMConsumerManager']]],
  ['favouritecontentlistwithcompletionblock_3a',['favouriteContentListWithCompletionBlock:',['../interface_v_m_consumer_manager.html#a54dd8def0565b259447ef6294d8f3552',1,'VMConsumerManager']]],
  ['features',['features',['../interface_v_m_venue.html#ad0245cdf15b6ed03d41ae9c3f86474a9',1,'VMVenue']]],
  ['feedbackwithrating_3afeedbacktext_3auniqueidentifier_3aextendeddata_3acompletionblock_3a',['feedbackWithRating:feedbackText:uniqueIdentifier:extendedData:completionBlock:',['../interface_v_m_consumer_manager.html#a165d3c771c98d9b08feef9bde312d2d3',1,'VMConsumerManager']]],
  ['fillmultiplecards',['fillMultipleCards',['../interface_v_m_add_points_info.html#aa13a68a7d8c2ae7e144fadd137452e6e',1,'VMAddPointsInfo']]],
  ['firstname',['firstName',['../interface_v_m_consumer.html#a0b2d5132e9dffce1533653d3eb00b83d',1,'VMConsumer']]],
  ['fullname',['fullName',['../interface_v_m_consumer.html#a8f2e5da61c7b20eeacb88c6c45bd2093',1,'VMConsumer']]]
];
