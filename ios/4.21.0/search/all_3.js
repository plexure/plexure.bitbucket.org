var searchData=
[
  ['dailyendtime',['dailyEndTime',['../interface_v_m_content_model.html#a07ad13775b8de49e03bef99f35be2c96',1,'VMContentModel']]],
  ['dailystarttime',['dailyStartTime',['../interface_v_m_content_model.html#a107634496f3a0e6e1de18882234996f3',1,'VMContentModel']]],
  ['datawithbase64string_3a',['dataWithBase64String:',['../category_n_s_data_07_v_m_toolkit_additions_08.html#a34d01127b3a2496e5bd39c50dfb2edaf',1,'NSData(VMToolkitAdditions)']]],
  ['datebyaddingdays_3a',['dateByAddingDays:',['../category_n_s_date_07_v_m_format_extensions_08.html#a96ee1df903a0b867b5e2afa24aa21985',1,'NSDate(VMFormatExtensions)']]],
  ['datebyaddingmonths_3a',['dateByAddingMonths:',['../category_n_s_date_07_v_m_format_extensions_08.html#aa6cdd06837dd8b7348e8ceb17afa6a21',1,'NSDate(VMFormatExtensions)']]],
  ['datebyaddingyears_3a',['dateByAddingYears:',['../category_n_s_date_07_v_m_format_extensions_08.html#a5b37809ba817630a9532b38355ae8129',1,'NSDate(VMFormatExtensions)']]],
  ['datecreated',['dateCreated',['../interface_v_m_advertisement.html#a4ba3b0f7873a8bd1e950a8206b747715',1,'VMAdvertisement']]],
  ['datefromstring_3awithformat_3a',['dateFromString:withFormat:',['../category_n_s_date_07_v_m_format_extensions_08.html#a2bf8a7bafaf58a1daf2e0d7572022a34',1,'NSDate(VMFormatExtensions)']]],
  ['datemodified',['dateModified',['../interface_v_m_advertisement.html#aee6378123d13f35f00a3a8a344566cdb',1,'VMAdvertisement']]],
  ['dateofbirth',['dateOfBirth',['../interface_v_m_consumer.html#afa64e09988a65247a1eb4a607642a71f',1,'VMConsumer']]],
  ['daterequested',['dateRequested',['../interface_v_m_loyalty_card_transaction.html#a771aba8dddc965508aad2536116ef955',1,'VMLoyaltyCardTransaction']]],
  ['dateutcstring_3a',['dateUTCString:',['../interface_v_m_utils.html#afe65f30ddc4414b20d65feae7ea3aa56',1,'VMUtils']]],
  ['datevaluewithformat_3a',['dateValueWithFormat:',['../category_n_s_string_07_v_m_additions_08.html#a804dd0d807b3dcde1ce2522c2c37079b',1,'NSString(VMAdditions)']]],
  ['datewithouttimecomponent',['dateWithoutTimeComponent',['../category_n_s_date_07_v_m_format_extensions_08.html#ad81ee01cfc359fd8bbd0242afb9e12a2',1,'NSDate(VMFormatExtensions)::dateWithoutTimeComponent()'],['../category_n_s_date_07_v_m_format_extensions_08.html#ad81ee01cfc359fd8bbd0242afb9e12a2',1,'NSDate(VMFormatExtensions)::dateWithoutTimeComponent()']]],
  ['day',['day',['../interface_v_m_venue_open_hours.html#a034d97e3d5b18ddc54b47067869288e5',1,'VMVenueOpenHours::day()'],['../category_n_s_date_07_v_m_format_extensions_08.html#a9223413cb40ed7c0697c5743074465c5',1,'NSDate(VMFormatExtensions)::day()']]],
  ['dayendtime',['dayEndTime',['../category_n_s_date_07_v_m_format_extensions_08.html#abd408e2f3d0eb4c12c3bc1d0a19a6a55',1,'NSDate(VMFormatExtensions)']]],
  ['daysofweek',['daysOfWeek',['../interface_v_m_advertisement.html#af3f93d32dcd52ee1a4161009aa837a41',1,'VMAdvertisement::daysOfWeek()'],['../interface_v_m_offer.html#a22faf06c6776b3203f27f09d5de43eb2',1,'VMOffer::daysOfWeek()']]],
  ['debug_3a',['debug:',['../interface_v_m_log.html#a5ebe6076b3c76d3c5049600a5222bf70',1,'VMLog']]],
  ['deleteconsumeraccountwithcompletionblock_3a',['deleteConsumerAccountWithCompletionBlock:',['../interface_v_m_authorization_manager.html#a7a8184a5230eeb635369d246c955775e',1,'VMAuthorizationManager']]],
  ['deleteconsumerwithcompletionblock_3a',['deleteConsumerWithCompletionBlock:',['../interface_v_m_consumer_manager.html#a2193fd4937ba95d294addc7b35c94a6f',1,'VMConsumerManager']]],
  ['deletedeviceaccountwithcompletionblock_3a',['deleteDeviceAccountWithCompletionBlock:',['../interface_v_m_authorization_manager.html#adc129d958f7361d25efe5b80701a92a2',1,'VMAuthorizationManager']]],
  ['deletedvenuesids',['deletedVenuesIds',['../interface_v_m_venues_changeset.html#a101d44a27ce4e6efd9bc078f4140a41c',1,'VMVenuesChangeset']]],
  ['deletepasswordforservice_3aaccount_3a',['deletePasswordForService:account:',['../interface_v_m_keychain.html#aac716057dacadea8031af35fd082d068',1,'VMKeychain']]],
  ['deletepasswordforservice_3aaccount_3aerror_3a',['deletePasswordForService:account:error:',['../interface_v_m_keychain.html#a410174aafab825910738d9ced66eb1ca',1,'VMKeychain']]],
  ['deleteredeemedofferswithofferid_3acompletionblock_3a',['deleteRedeemedOffersWithOfferId:completionBlock:',['../interface_v_m_offer_manager.html#a9849545c7522713987258dec72a7bb7c',1,'VMOfferManager']]],
  ['deprecated_20list',['Deprecated List',['../deprecated.html',1,'']]],
  ['deprecated_5fmsg_5fattribute',['DEPRECATED_MSG_ATTRIBUTE',['../interface_v_m_advertisement.html#a9bdb26c9e61ca39e7fa783626192f1da',1,'VMAdvertisement::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_advertisement.html#a9bdb26c9e61ca39e7fa783626192f1da',1,'VMAdvertisement::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_authorization_manager.html#adeb9645649422dd1f9e56cdbbad82c32',1,'VMAuthorizationManager::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_authorization_manager.html#a44c478f013649d681186fadb1ccd698a',1,'VMAuthorizationManager::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_configuration_manager.html#aafa5373d8eae310b602037bca199c966',1,'VMConfigurationManager::DEPRECATED_MSG_ATTRIBUTE(&quot;It might be removed in the coming releases.&quot;)'],['../interface_v_m_configuration_manager.html#aa9fd5ab16db31f5b40f63e4cc7cda314',1,'VMConfigurationManager::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_configuration_manager.html#aa9fd5ab16db31f5b40f63e4cc7cda314',1,'VMConfigurationManager::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_mob.html#a73e8b71bbb2f21ef945a48cdfec43002',1,'VMob::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_mob.html#a73e8b71bbb2f21ef945a48cdfec43002',1,'VMob::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_offer.html#ac62e5fb4265c6f3bdaba8a71a9ac62b7',1,'VMOffer::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_offer.html#ac62e5fb4265c6f3bdaba8a71a9ac62b7',1,'VMOffer::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_offer.html#aff46abf8e7d790a338a4052bab569648',1,'VMOffer::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_offer.html#aff46abf8e7d790a338a4052bab569648',1,'VMOffer::DEPRECATED_MSG_ATTRIBUTE()'],['../interface_v_m_utils.html#a643131ec50c8b38b08ce7257f95effc0',1,'VMUtils::DEPRECATED_MSG_ATTRIBUTE()']]],
  ['desc',['desc',['../interface_v_m_external_merchant.html#a33a15b20f2d7b7c294284d1cd2425d42',1,'VMExternalMerchant']]],
  ['descriptionforapplicationstate',['descriptionForApplicationState',['../interface_v_m_utils.html#ae0c3ee9f9d0b8a742aebac31f8d81a62',1,'VMUtils']]],
  ['detachsocialaccountwithsocialsource_3acompletionblock_3a',['detachSocialAccountWithSocialSource:completionBlock:',['../interface_v_m_authorization_manager.html#a6066129fb68b8581515cff72815b0f5d',1,'VMAuthorizationManager']]],
  ['detailconsumerwithcompletionblock_3a',['detailConsumerWithCompletionBlock:',['../interface_v_m_consumer_manager.html#a6c5573cb5b9ed7d51f5d8586b414e4bb',1,'VMConsumerManager']]],
  ['detailsofcity_3awithcompletionblock_3a',['detailsOfCity:withCompletionBlock:',['../interface_v_m_locations_manager.html#aa3f66d71a2e0b869c7dff076eccabae2',1,'VMLocationsManager']]],
  ['detailsofstate_3awithcompletionblock_3a',['detailsOfState:withCompletionBlock:',['../interface_v_m_locations_manager.html#a6a83f76dcba7638e1fc69532f806367b',1,'VMLocationsManager']]],
  ['devicepassword',['devicePassword',['../interface_v_m_utils.html#aa5e2a734f0ac61e9b8d375879a284e79',1,'VMUtils::devicePassword()'],['../interface_v_m_utils.html#a462ca48f2ae9e2b749fb5e8445faa430',1,'VMUtils::devicePassword()']]],
  ['devicescreenresolution',['deviceScreenResolution',['../interface_v_m_utils.html#ad6af32b6f06ee839f3f1a915529da85e',1,'VMUtils']]],
  ['devicetype',['deviceType',['../interface_v_m_utils.html#a93bd4399a5d1014d5a49cc537ab4e1c9',1,'VMUtils']]],
  ['deviceusername',['deviceUsername',['../interface_v_m_utils.html#a39ba806d333ab8ffbcd1447475e8087c',1,'VMUtils::deviceUsername()'],['../interface_v_m_utils.html#a3c9044575e64a09f71538e8337035a07',1,'VMUtils::deviceUsername()']]],
  ['dictionaryheader',['dictionaryHeader',['../interface_v_m_base_request.html#af91a3181c9492c9a7334e2646076bb59',1,'VMBaseRequest']]],
  ['dictionaryqueryparam',['dictionaryQueryParam',['../interface_v_m_base_request.html#af146004e6be7b9f4821a86a8077ff701',1,'VMBaseRequest']]],
  ['disconnectsocialaccountwithsocialtypes_3acompletionblock_3a',['disconnectSocialAccountWithSocialTypes:completionBlock:',['../interface_v_m_consumer_manager.html#a9887bd26233ff100d830ec46721a11e5',1,'VMConsumerManager']]],
  ['distancefromcurrentlocation',['distanceFromCurrentLocation',['../interface_v_m_venue.html#a6c424132603fc42126a5c0ec22a8f524',1,'VMVenue']]],
  ['distancetoclosestvenue',['distanceToClosestVenue',['../interface_v_m_advertisement.html#a15304c488d245b43d8cc8319fecc9c61',1,'VMAdvertisement::distanceToClosestVenue()'],['../interface_v_m_offer.html#a3e739732ad052614b5ffc4f163969031',1,'VMOffer::distanceToClosestVenue()']]]
];
