var searchData=
[
  ['gender',['gender',['../interface_v_m_consumer.html#a5887766f9fe7afd4196a146ceb96cec4',1,'VMConsumer']]],
  ['geofenceid',['geofenceId',['../interface_v_m_geo_fence.html#ab8b311cbf1f6de27c1392210954a7cb4',1,'VMGeoFence']]],
  ['giftbatchid',['giftBatchId',['../interface_v_m_offer.html#af1e717ac307dbb25d7e2b09a4fc7d36e',1,'VMOffer::giftBatchId()'],['../interface_v_m_redeemed_offer.html#a1262aa45eac067921c28ff483597e44f',1,'VMRedeemedOffer::giftBatchId()']]],
  ['giftedby',['giftedBy',['../interface_v_m_offer.html#a2e212ab0072735cc782c1c7de077d87c',1,'VMOffer']]],
  ['giftedbyconsumerid',['giftedByConsumerId',['../interface_v_m_redeemed_offer.html#a0cc833d3279fb3405e40b59f470dece0',1,'VMRedeemedOffer']]],
  ['giftedcopy',['giftedCopy',['../interface_v_m_offer.html#a7c8d1704dc66880899646f5b01a4badb',1,'VMOffer']]],
  ['giftedondate',['giftedOnDate',['../interface_v_m_offer.html#a66516d0a8c91a185014886354870ba38',1,'VMOffer::giftedOnDate()'],['../interface_v_m_redeemed_offer.html#a2b43a7f42486ab9b8c492beb3ba701db',1,'VMRedeemedOffer::giftedOnDate()']]],
  ['giftid',['giftId',['../interface_v_m_offer.html#ae4e24bbf47fbf0269c1dc28e86cd50b8',1,'VMOffer::giftId()'],['../interface_v_m_redeemed_offer.html#aa28dd8679736096ac2789de22e794213',1,'VMRedeemedOffer::giftId()']]]
];
