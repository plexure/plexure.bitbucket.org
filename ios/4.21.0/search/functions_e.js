var searchData=
[
  ['redeemofferwithofferid_3agiftedid_3aofferinstanceuniqueid_3acompletionblock_3a',['redeemOfferWithOfferId:giftedID:offerInstanceUniqueId:completionBlock:',['../interface_v_m_offer_manager.html#aed4d20a5db93af147b6ba0f974faca49',1,'VMOfferManager']]],
  ['removetagvaluereferencecodes_3acompletionblock_3a',['removeTagValueReferenceCodes:completionBlock:',['../interface_v_m_consumer_manager.html#a59d0a9380c291507b34e5560d94c5ffc',1,'VMConsumerManager']]],
  ['requesterrorwithcode_3adescription_3a',['requestErrorWithCode:description:',['../interface_v_m_request_error.html#a2b9ddd14236c8c55fdf604817da81914',1,'VMRequestError']]],
  ['requestpasswordresetwithusername_3acompletionblock_3a',['requestPasswordResetWithUsername:completionBlock:',['../interface_v_m_authorization_manager.html#a83ab307f5aab2e54f31d20bd0588da4a',1,'VMAuthorizationManager']]],
  ['resetpassword_3averificationtoken_3acompletionblock_3a',['resetPassword:verificationToken:completionBlock:',['../interface_v_m_authorization_manager.html#a115fc2875c63fae2fadeb70da56b7663',1,'VMAuthorizationManager']]]
];
