var searchData=
[
  ['offer',['offer',['../interface_v_m_weighted_content.html#a4bfca7a04d3c4c3b708cb293160b1cc0',1,'VMWeightedContent']]],
  ['offerid',['offerId',['../interface_v_m_redeemed_offer.html#a51e99c61370d653edb33f8491af20432',1,'VMRedeemedOffer']]],
  ['offerinstanceid',['offerInstanceId',['../interface_v_m_loyalty_card_allocated_instance.html#a6691131710123ee906f891b8369ea096',1,'VMLoyaltyCardAllocatedInstance']]],
  ['offerinstanceuniqueid',['offerInstanceUniqueId',['../interface_v_m_offer.html#a552ef387b38cd69b558f78eee789652c',1,'VMOffer::offerInstanceUniqueId()'],['../interface_v_m_redeemed_offer.html#abf3d66200589941fd0bf6418341c1dbc',1,'VMRedeemedOffer::offerInstanceUniqueId()']]],
  ['offers',['offers',['../interface_v_m_favourite_content.html#aba4aedeb8229cc36f8fdefea63e06c40',1,'VMFavouriteContent::offers()'],['../interface_v_m_loyalty_card.html#a41927f5b75927e38720e10cec2f63465',1,'VMLoyaltyCard::offers()'],['../interface_v_m_venue.html#a1ff0b8c0a0d7abf894e322c38e962db7',1,'VMVenue::offers()']]],
  ['offset',['offset',['../interface_v_m_venue_search_criteria.html#a157870ac184beb205e46679b37586dba',1,'VMVenueSearchCriteria::offset()'],['../interface_v_m_weighted_content_list_criteria.html#a2fe594d58c724ec1d8710b8e551c6911',1,'VMWeightedContentListCriteria::offset()']]],
  ['operation',['operation',['../interface_v_m_service_request.html#a635797b2646913e2b81e075690d6c1b2',1,'VMServiceRequest']]],
  ['organistationtype',['organistationType',['../interface_v_m_merchant.html#af003eef7f6f9a4caba92dd6a8ab60319',1,'VMMerchant']]],
  ['outcomecode',['outcomeCode',['../interface_v_m_loyalty_card_allocated_instance.html#a1a63fa6abc3372b614e304a6dd93819f',1,'VMLoyaltyCardAllocatedInstance']]],
  ['outcomedescription',['outcomeDescription',['../interface_v_m_loyalty_card_allocated_instance.html#a6c52f55d5e2037c822dd1ca5175cf684',1,'VMLoyaltyCardAllocatedInstance']]]
];
