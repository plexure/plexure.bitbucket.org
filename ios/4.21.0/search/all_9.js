var searchData=
[
  ['lastburntat',['lastBurntAt',['../interface_v_m_offer.html#a7358dcc9dbfef63f6403e1a71a287f3b',1,'VMOffer']]],
  ['lastname',['lastName',['../interface_v_m_consumer.html#aead6cd72e1c0791bdaf7eb0143726871',1,'VMConsumer']]],
  ['lastredeemedat',['lastRedeemedAt',['../interface_v_m_offer.html#adcaf26570b73cb5d9d7fddfc829430ef',1,'VMOffer']]],
  ['lastsendstack',['lastSendStack',['../interface_v_m_activity_manager.html#accc46f56266f09c3c741fc971838f91b',1,'VMActivityManager']]],
  ['lastupdateconfiguration',['lastUpdateConfiguration',['../interface_v_m_utils.html#ac4ddbc109de86d922d7dd653fefec7e7',1,'VMUtils']]],
  ['lastupdatedat',['lastUpdatedAt',['../interface_v_m_offer.html#ae13d1a1a1ca2287e04c215b9e672e512',1,'VMOffer::lastUpdatedAt()'],['../interface_v_m_redeemed_offer.html#a4bd3c0603f066b2d88e0833214964f85',1,'VMRedeemedOffer::lastUpdatedAt()']]],
  ['lastupdatedate',['lastUpdateDate',['../interface_v_m_venue_search_criteria.html#a35b5273510d03396b2e59b423de52cd5',1,'VMVenueSearchCriteria']]],
  ['latestcachedlocation',['latestCachedLocation',['../interface_v_m_base_location_manager.html#a74294f88e1258612d7d713e340683191',1,'VMBaseLocationManager']]],
  ['latitude',['latitude',['../interface_v_m_city.html#a88cc6203d099e4c8d09a49a77285b5cb',1,'VMCity::latitude()'],['../interface_v_m_geo_fence.html#a9764dd5d2bba10b29a1a0e48850faaa7',1,'VMGeoFence::latitude()'],['../interface_v_m_venue.html#ab335e8e524b8468b76f511392d8db4ce',1,'VMVenue::latitude()']]],
  ['limit',['limit',['../interface_v_m_ad_search_criteria.html#a695fe3ee98886f951a8442c1b0e0edca',1,'VMAdSearchCriteria::limit()'],['../interface_v_m_venue_search_criteria.html#aebd6c7a0c115c7c9ac834b9fe0345533',1,'VMVenueSearchCriteria::limit()']]],
  ['linktoexistingconsumer',['linkToExistingConsumer',['../interface_v_m_social_login_info.html#ac866997399ac06ca38c99bfe7067dbd9',1,'VMSocialLoginInfo']]],
  ['linktoexternalsystemwithexternalid_3asystemtype_3acompletionblock_3a',['linkToExternalSystemWithExternalId:systemType:completionBlock:',['../interface_v_m_authorization_manager.html#a06d790d52ab4c86bba324d280ff962b3',1,'VMAuthorizationManager']]],
  ['listpointstransactionswithloyaltycardid_3astartdate_3aenddate_3acompletionblock_3a',['listPointsTransactionsWithLoyaltyCardId:startDate:endDate:completionBlock:',['../interface_v_m_loyalty_cards_manager.html#adf7f42634392d6cccd9ee3f693e5870f',1,'VMLoyaltyCardsManager']]],
  ['listredeemedofferswithcompletionblock_3a',['listRedeemedOffersWithCompletionBlock:',['../interface_v_m_offer_manager.html#ae1dfc650fb7f3b302003888bf5af31ae',1,'VMOfferManager']]],
  ['listredeemedofferswithignorestartenddates_3acompletionblock_3a',['listRedeemedOffersWithIgnoreStartEndDates:completionBlock:',['../interface_v_m_offer_manager.html#a7fe69b396e443a36428e5f83cc926652',1,'VMOfferManager']]],
  ['listtagwithcompletionblock_3a',['listTagWithCompletionBlock:',['../interface_v_m_consumer_manager.html#aeb0434a0468a29ca46364b90ddbec47b',1,'VMConsumerManager']]],
  ['localformatdate_3a',['localFormatDate:',['../interface_v_m_utils.html#a2033d0521d7dca0384a5f99a40692578',1,'VMUtils']]],
  ['locationaccuracy',['locationAccuracy',['../interface_v_m_utils.html#a1813ef0f8e37609af79b7280701e048f',1,'VMUtils']]],
  ['locationapiurl',['locationApiUrl',['../interface_v_m_utils.html#a55155bbe16b2fd8a8cbc2267e3fc3fbe',1,'VMUtils']]],
  ['locationdistancefilter',['locationDistanceFilter',['../interface_v_m_utils.html#af4d0c5b9b931c854b0f0a6f59392f0c8',1,'VMUtils']]],
  ['locationmanager',['locationManager',['../interface_v_m_base_location_manager.html#a84b9740371a908e90bac115b3a4a4351',1,'VMBaseLocationManager']]],
  ['locationsmanager',['locationsManager',['../interface_v_mob.html#a1a9c4092de09755313413f22c80ae731',1,'VMob']]],
  ['logginglevel',['loggingLevel',['../interface_v_m_utils.html#af8dedfccaddf2e4a1ed59c6c78496892',1,'VMUtils']]],
  ['loginwithsociallogininfo_3acompletionblock_3a',['loginWithSocialLogininfo:completionBlock:',['../interface_v_m_authorization_manager.html#a88bb99882e857129fb80201b14d3f2c7',1,'VMAuthorizationManager']]],
  ['loginwithsocialtoken_3adefaulttoconsumer_3adefaulttoconsumerusername_3adefaulttoconsumerpassword_3asocialtype_3acompletionblock_3a',['loginWithSocialToken:defaultToConsumer:defaultToConsumerUsername:defaultToConsumerPassword:socialType:completionBlock:',['../interface_v_m_authorization_manager.html#a0cfa73293e47a3a2dec3b36673c38dc3',1,'VMAuthorizationManager']]],
  ['loginwithusername_3apassword_3acompletionblock_3a',['loginWithUserName:password:completionBlock:',['../interface_v_m_authorization_manager.html#ab160839587963ac102d0d871f48641b8',1,'VMAuthorizationManager']]],
  ['loginwithusername_3apassword_3areturncrossreferences_3areturnconsumerinfo_3acompletionblock_3a',['loginWithUserName:password:returnCrossReferences:returnConsumerInfo:completionBlock:',['../interface_v_m_authorization_manager.html#a407a54ed9953b48f29509baca6a14511',1,'VMAuthorizationManager']]],
  ['loginwithusername_3apassword_3areturncrossreferences_3areturnconsumerinfo_3averificationtoken_3acompletionblock_3a',['loginWithUserName:password:returnCrossReferences:returnConsumerInfo:verificationToken:completionBlock:',['../interface_v_m_authorization_manager.html#aae681bd6cc93b497a03f2479c4707f86',1,'VMAuthorizationManager']]],
  ['loginwithusername_3apassword_3averificationtoken_3acompletionblock_3a',['loginWithUserName:password:verificationToken:completionBlock:',['../interface_v_m_authorization_manager.html#a9cf00e67bd49578043c3a89cb86836f4',1,'VMAuthorizationManager']]],
  ['logoutwithcompletionblock_3a',['logoutWithCompletionBlock:',['../interface_v_m_authorization_manager.html#a459ab6744bce2dbbc24ca481b10558b7',1,'VMAuthorizationManager']]],
  ['longitude',['longitude',['../interface_v_m_city.html#a432b16acb1d6b637cc74bf19971d6fa4',1,'VMCity::longitude()'],['../interface_v_m_geo_fence.html#a389e683464a6f3093bec56fb9c7b70ad',1,'VMGeoFence::longitude()'],['../interface_v_m_venue.html#a6efb78ad3088f8396e7c3df636c89c92',1,'VMVenue::longitude()']]],
  ['lowresolutionimage',['lowResolutionImage',['../interface_v_m_categories.html#aae6719c9fdd88161e07b7ec2765ff067',1,'VMCategories']]],
  ['lowresolutionselectedimage',['lowResolutionSelectedImage',['../interface_v_m_categories.html#a8ef0ab6b0cb6690efb3da9cdb0509f2a',1,'VMCategories']]],
  ['loyaltycardid',['loyaltyCardId',['../interface_v_m_loyalty_card_create_points.html#adafbc6535397474a93a59957e1a78288',1,'VMLoyaltyCardCreatePoints::loyaltyCardId()'],['../interface_v_m_loyalty_card_instance.html#a9486f4ecc5424d5c5a532fd416a85777',1,'VMLoyaltyCardInstance::loyaltyCardId()'],['../interface_v_m_loyalty_card_transaction.html#a2ceaabbdb09aee3dcb1ea151e65b0aae',1,'VMLoyaltyCardTransaction::loyaltyCardId()']]],
  ['loyaltycardimpressionwithloyaltycardid_3a',['loyaltyCardImpressionWithLoyaltyCardId:',['../interface_v_m_activity_manager.html#ae60027b13291ef08f0a3addb6cfcf360',1,'VMActivityManager']]],
  ['loyaltycardinstanceid',['loyaltyCardInstanceId',['../interface_v_m_loyalty_card_allocated_instance.html#a5e3722977b1092b8b0ae6d87ed7d333f',1,'VMLoyaltyCardAllocatedInstance::loyaltyCardInstanceId()'],['../interface_v_m_loyalty_card_instance.html#ac6179a778e57d9f7e4b4cd2638ecbff7',1,'VMLoyaltyCardInstance::loyaltyCardInstanceId()'],['../interface_v_m_loyalty_card_transaction.html#a14ce6e5de9825ee98c6115f7726c7320',1,'VMLoyaltyCardTransaction::loyaltyCardInstanceId()']]],
  ['loyaltycardlistwithcompletionblock_3a',['loyaltyCardListWithCompletionBlock:',['../interface_v_m_loyalty_cards_manager.html#ae9714362549297b941b888dd076f0b0e',1,'VMLoyaltyCardsManager::loyaltyCardListWithCompletionBlock:()'],['../interface_v_m_offer_manager.html#a5cf8092ac20d6e696cb30bd5dc03175c',1,'VMOfferManager::loyaltyCardListWithCompletionBlock:()']]],
  ['loyaltycardsmanager',['loyaltyCardsManager',['../interface_v_mob.html#a3de975b77a4f9ec2379d509cddc2afcb',1,'VMob']]]
];
