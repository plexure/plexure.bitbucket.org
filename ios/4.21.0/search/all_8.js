var searchData=
[
  ['ignoredailytimefilter',['ignoreDailyTimeFilter',['../interface_v_m_ad_search_criteria.html#a0c2b5ce2645324e072517f1ddd382aa5',1,'VMAdSearchCriteria']]],
  ['imagealt',['imageAlt',['../interface_v_m_offer.html#a36cf3c77f5ec58e126033b727daa3676',1,'VMOffer']]],
  ['imagealtdescription',['imageAltDescription',['../interface_v_m_offer.html#a53e8f79ebd8a1414c7126480bf53801c',1,'VMOffer']]],
  ['imagedescription',['imageDescription',['../interface_v_m_content_model.html#a4c8a98e9d5236227689c3e5f181bbe17',1,'VMContentModel']]],
  ['imageurlstringwithpath_3awidth_3aheight_3aimageformat_3aandurlprefix_3a',['imageUrlStringWithPath:width:height:imageFormat:andUrlPrefix:',['../interface_v_m_utils.html#a22cd698e2f4729ceb38511ee34045eeb',1,'VMUtils']]],
  ['inactivereasons',['inactiveReasons',['../interface_v_m_loyalty_card.html#aeb8b454bcd56a26cc9203164b7935352',1,'VMLoyaltyCard']]],
  ['include',['include',['../interface_v_m_weighted_content_list_criteria.html#a115ef58ea51fc6ad1b56579c6403af1d',1,'VMWeightedContentListCriteria']]],
  ['info_3a',['info:',['../interface_v_m_log.html#ac60228a0d9a834927dae56d364f10300',1,'VMLog']]],
  ['initadsearchcriteriawithchannel_3aplacement_3alimit_3aignoredailytimefilter_3a',['initAdSearchCriteriaWithChannel:placement:limit:ignoreDailyTimeFilter:',['../interface_v_m_ad_search_criteria.html#a75bc5f518b2e9f3bce07ad2d2a5d7459',1,'VMAdSearchCriteria']]],
  ['initialize',['initialize',['../interface_v_mob.html#a7956cc23c28530f846c31103c06d3f95',1,'VMob']]],
  ['initialpoints',['initialPoints',['../interface_v_m_loyalty_card.html#a9baabe2bad88fa91e665794c159c757a',1,'VMLoyaltyCard']]],
  ['initoffersearchcriteriawithkeyword_3acategoryid_3amerchantid_3aorderby_3atagsfilterexpression_3aoffset_3alimit_3aignoredailytimefilter_3aignoredayfilter_3arankedsearch_3aascending_3afields_3a',['initOfferSearchCriteriaWithKeyword:categoryId:merchantId:orderBy:tagsFilterExpression:offset:limit:ignoreDailyTimeFilter:ignoreDayFilter:rankedSearch:ascending:fields:',['../interface_v_m_offer_search_criteria.html#a38fbdf5a40426f356f7d32bc4233aea0',1,'VMOfferSearchCriteria']]],
  ['initsignupinfowithsignupparams_3aexternalid_3asystemtype_3averificationtoken_3a',['initSignUpInfoWithSignUpParams:externalId:systemType:verificationToken:',['../interface_v_m_sign_up_info.html#a379390c8800c7e3834ea222bc7f48a99',1,'VMSignUpInfo']]],
  ['initsignupparamswithtype_3aemailaddressorphonenumber_3ausername_3apassword_3afirstname_3alastname_3afullname_3agender_3adateofbirth_3acity_3aextendeddata_3a',['initSignUpParamsWithType:emailAddressOrPhoneNumber:username:password:firstName:lastName:fullName:gender:dateOfBirth:city:extendedData:',['../interface_v_m_sign_up_params.html#ae919a766672c19e78ebfb867a59172af',1,'VMSignUpParams']]],
  ['initsociallogininfowiththirdpartytoken_3aexistingconsumerusername_3aexistingconsumerpassword_3asocialsource_3aautocreateconsumer_3a',['initSocialLoginInfoWithThirdPartyToken:existingConsumerUsername:existingConsumerPassword:socialSource:autoCreateConsumer:',['../interface_v_m_social_login_info.html#a676fc7366ec208d54a58c785f8a6180e',1,'VMSocialLoginInfo']]],
  ['initvenuesearchcriteriawithlimit_3aoffset_3alastupdateddate_3amerchantid_3a',['initVenueSearchCriteriaWithlimit:offset:lastUpdatedDate:merchantId:',['../interface_v_m_venue_search_criteria.html#aaa6313d760203f6107841b89c1623d01',1,'VMVenueSearchCriteria']]],
  ['initweightedcontentsearchcriteriawithcategoryid_3alatitude_3alongitude_3atagsfilterexpression_3aignoredailytimefilter_3aincludevenuerelatedoffers_3ainclude_3adatetime_3aregionid_3afilterbyvenueid_3aoffset_3alimit_3a',['initWeightedContentSearchCriteriaWithCategoryId:latitude:longitude:tagsFilterExpression:ignoreDailyTimeFilter:includeVenueRelatedOffers:include:dateTime:regionId:filterByVenueId:offset:limit:',['../interface_v_m_weighted_content_list_criteria.html#ae4545c1746856e349bdbbec4802835d3',1,'VMWeightedContentListCriteria']]],
  ['initwithcsvstring_3a',['initWithCSVString:',['../interface_v_m_beacon.html#aa27b4c90b9649c9bcdfd7ec993b8b978',1,'VMBeacon']]],
  ['initwithdictionary_3a',['initWithDictionary:',['../interface_v_m_base_model.html#ad7f523424b63ff66f4864e14bae22216',1,'VMBaseModel']]],
  ['initwithloyaltycardid_3apoints_3atransactionid_3aautoactivatereward_3afillmultiplecards_3aextendeddata_3a',['initWithLoyaltyCardId:points:transactionId:autoActivateReward:fillMultipleCards:extendedData:',['../interface_v_m_add_points_info.html#a32889666c189ce3e7b67cef1cf472de4',1,'VMAddPointsInfo']]],
  ['initwithtoken_3a',['initWithToken:',['../interface_v_m_base_request.html#a09ce14725e6a949248b637840bcf3dca',1,'VMBaseRequest']]],
  ['instances',['instances',['../interface_v_m_loyalty_card.html#a2f60eaa1afe3e86e2e273e2364669e19',1,'VMLoyaltyCard']]],
  ['instancesavailable',['instancesAvailable',['../interface_v_m_loyalty_card.html#ac522eaef58dee6ec7396ff90848d9517',1,'VMLoyaltyCard']]],
  ['instructions',['instructions',['../interface_v_m_loyalty_card.html#af9f8cb35447a2eeb4b5a672e9227c798',1,'VMLoyaltyCard']]],
  ['isactive',['isActive',['../interface_v_m_advertisement.html#a661e65560c9b96598f11111eed77fb37',1,'VMAdvertisement::isActive()'],['../interface_v_m_loyalty_card.html#acad0542dfa0c5ab1b6dad2575e41f696',1,'VMLoyaltyCard::isActive()'],['../interface_v_m_offer.html#aa048d72ee0cd93c09859e86552766e55',1,'VMOffer::isActive()']]],
  ['isactivitiestrackingenabled',['isActivitiesTrackingEnabled',['../interface_v_m_configuration_manager.html#aef022bdd4a80d002a8d7ccc43e4bb51f',1,'VMConfigurationManager']]],
  ['isagift',['isAGift',['../interface_v_m_offer.html#a1d0065408ffaf3f0f2b9833d0ae3d6a5',1,'VMOffer::isAGift()'],['../interface_v_m_redeemed_offer.html#a937e19aa4d29338d2cbbb39faacd5506',1,'VMRedeemedOffer::isAGift()']]],
  ['isapnscrossreferenceupdated_3a',['isAPNSCrossReferenceUpdated:',['../interface_v_m_a_p_n_s_utils.html#abeab61a8a204173b29ced9338f48bd34',1,'VMAPNSUtils']]],
  ['isauthenticated',['isAuthenticated',['../interface_v_m_authorization_manager.html#a6c2d9007016b7d2f7cd146bd28c0f524',1,'VMAuthorizationManager']]],
  ['isbeaconsmonitoringenabled',['isBeaconsMonitoringEnabled',['../interface_v_m_configuration_manager.html#a402ed4670890c56ac39c6ebdc9eb34b5',1,'VMConfigurationManager']]],
  ['isburnt',['isBurnt',['../interface_v_m_redeemed_offer.html#a84152df0050e8e15c751986d71673b1b',1,'VMRedeemedOffer']]],
  ['iscancelled',['isCancelled',['../interface_v_m_operation.html#a2078dc025ca61927efe3b52d8736e905',1,'VMOperation']]],
  ['isgeofencesmonitoringenabled',['isGeofencesMonitoringEnabled',['../interface_v_m_configuration_manager.html#a896bd7c8c9b0a78f2a906c0c477da023',1,'VMConfigurationManager']]],
  ['isgiftable',['isGiftable',['../interface_v_m_offer.html#a3e129a356012b5997bf560ee8dbffa6c',1,'VMOffer']]],
  ['ishidden',['isHidden',['../interface_v_m_external_merchant.html#a351e0be79b7fdd97d06affad50c62410',1,'VMExternalMerchant']]],
  ['isinitialized',['isInitialized',['../interface_v_mob.html#a1ac7c5eedef2e2f2c81338b730dd9f43',1,'VMob']]],
  ['isloggingenabled',['isLoggingEnabled',['../interface_v_m_utils.html#a7dcb1112913d14702f9e295bce1b0fed',1,'VMUtils']]],
  ['isloggingtofile',['isLoggingToFile',['../interface_v_m_utils.html#a92251c84ba04c236f0b4b2c0c1431526',1,'VMUtils']]],
  ['ismerchantfavourite',['isMerchantFavourite',['../interface_v_m_offer.html#add14e172dac6f11336450bf50e6e8a3d',1,'VMOffer']]],
  ['isnotempty',['isNotEmpty',['../category_n_s_string_07_v_m_additions_08.html#a9a54345af541cc40c19c0b0bc6dbb771',1,'NSString(VMAdditions)']]],
  ['isnumeric',['isNumeric',['../category_n_s_string_07_v_m_validation_08.html#afccc8bd8db5451aaedff774275b3664a',1,'NSString(VMValidation)']]],
  ['ispremiumplacement',['isPremiumPlacement',['../interface_v_m_offer.html#ac82a112d425fde3206a27d9253749e9d',1,'VMOffer']]],
  ['isrespawningoffer',['isRespawningOffer',['../interface_v_m_offer.html#a64b3e4e22279083efb7658feb9b066ce',1,'VMOffer']]],
  ['isreward',['isReward',['../interface_v_m_offer.html#af1ad0988b7c833d54be0314ac155b634',1,'VMOffer::isReward()'],['../interface_v_m_redeemed_offer.html#af501a792b7a75fd31bdc419170489118',1,'VMRedeemedOffer::isReward()']]],
  ['isupdateconfiguration',['isUpdateConfiguration',['../interface_v_m_utils.html#ad6e183075bbe847fae16acc86e654a37',1,'VMUtils']]],
  ['isupdatinglocation',['isUpdatingLocation',['../interface_v_m_locations_manager.html#aaac02a259a2095c18940b9f458a66ed7',1,'VMLocationsManager']]],
  ['isvalid',['isValid',['../category_n_s_string_07_v_m_validation_08.html#aea198769e2b64beff986f0a526e706b0',1,'NSString(VMValidation)::isValid()'],['../category_c_l_location_07_validation_08.html#a8472fe430268ebc51d2fbf8a536fd2cd',1,'CLLocation(Validation)::isValid()']]],
  ['isvalidemail',['isValidEmail',['../category_n_s_string_07_v_m_validation_08.html#a11e3c5fea91ee320cbac444eac50b5e3',1,'NSString(VMValidation)']]],
  ['isvalidlocation_3a',['isValidLocation:',['../interface_v_m_base_location_manager.html#aafb8ea2a2f7e8e0c3ce4164c300fdc5e',1,'VMBaseLocationManager']]]
];
