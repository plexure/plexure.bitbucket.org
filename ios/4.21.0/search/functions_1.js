var searchData=
[
  ['base64encodedstring',['base64EncodedString',['../category_n_s_data_07_v_m_toolkit_additions_08.html#a2c4926f3e27a9f38a58a44fa9dc87846',1,'NSData(VMToolkitAdditions)::base64EncodedString()'],['../category_n_s_string_07_v_m_toolkit_additions_08.html#ada3f8f602c548bed493dc19856a8fb89',1,'NSString(VMToolkitAdditions)::base64EncodedString()']]],
  ['beaconmonitoringenabled',['beaconMonitoringEnabled',['../interface_v_m_utils.html#adb983262672079bff3a944c6197a1412',1,'VMUtils']]],
  ['boolforkey_3a',['boolForKey:',['../category_n_s_dictionary_07_v_m_validation_08.html#ad0bada6ba17534f0f60446be84151807',1,'NSDictionary(VMValidation)']]],
  ['buildversion',['buildVersion',['../interface_v_m_utils.html#ae3d1b92281a60e34db2a628df0449cbf',1,'VMUtils']]],
  ['buttonclickwithbuttonid_3amerchantid_3avenueid_3aitemid_3aitemcode_3a',['buttonClickWithButtonId:merchantId:venueId:itemId:itemCode:',['../interface_v_m_activity_manager.html#a43b9ee95e87e75889c178e56753f3546',1,'VMActivityManager']]]
];
