var searchData=
[
  ['radius',['radius',['../interface_v_m_geo_fence.html#a3672e44f46f99e28c0fa3ff2d679e7db',1,'VMGeoFence']]],
  ['redeemedat',['redeemedAt',['../interface_v_m_redeemed_offer.html#a0fbbc20a34586aefd1fa1ced82196f54',1,'VMRedeemedOffer']]],
  ['redeemedofferid',['redeemedOfferId',['../interface_v_m_loyalty_card_instance.html#a090795f29489217028762ef2cdd4e515',1,'VMLoyaltyCardInstance']]],
  ['redemptioncount',['redemptionCount',['../interface_v_m_offer.html#ae556948747419783b8fbc1ce2f11605d',1,'VMOffer']]],
  ['redemptionexpirydate',['redemptionExpiryDate',['../interface_v_m_redeemed_offer.html#ae6acd37c5bc9e68616a144f57ed91752',1,'VMRedeemedOffer']]],
  ['redemptiontext',['redemptionText',['../interface_v_m_redeemed_offer.html#ae90f17b37a61b8ef87eb36c3ae1c5bec',1,'VMRedeemedOffer']]],
  ['redemptiontype',['redemptionType',['../interface_v_m_offer.html#ab3ed9359af484f2df9877a1bf3bdd9b7',1,'VMOffer::redemptionType()'],['../interface_v_m_redeemed_offer.html#a430eae84e1fe0e26714a97de0672a022',1,'VMRedeemedOffer::redemptionType()']]],
  ['regionid',['regionId',['../interface_v_m_venue.html#a9607ee40863485d7a50aecf922c061a7',1,'VMVenue']]],
  ['requestdictionary',['requestDictionary',['../interface_v_m_service_request.html#a5b1f0d4752942d4d6891dd78d42384ef',1,'VMServiceRequest']]],
  ['respawnsindays',['respawnsInDays',['../interface_v_m_offer.html#a73a0c632a2405ff668815a8bc58d238f',1,'VMOffer']]],
  ['responsedata',['responseData',['../interface_v_m_service_request.html#afc710d88572aa47e5ca91ed390c86736',1,'VMServiceRequest']]],
  ['responseobject',['responseObject',['../interface_v_m_base_model.html#a46ba3c23b8346c729c0d1e95bfd47545',1,'VMBaseModel']]],
  ['rssi',['rssi',['../interface_v_m_beacon.html#adef762c5ccdf491ab8417657420b4d1e',1,'VMBeacon']]]
];
