var searchData=
[
  ['name',['name',['../interface_v_m_beacon_group.html#ac8411680b4369c31e397acb4f02e62ab',1,'VMBeaconGroup::name()'],['../interface_v_m_categories.html#aebfde083d7d3ada28e32a47ab46c5bee',1,'VMCategories::name()'],['../interface_v_m_city.html#a35b29053ff6cb684e711a20781911df5',1,'VMCity::name()'],['../interface_v_m_external_merchant.html#a2f196859b8f10f1c0d7b3d592269827f',1,'VMExternalMerchant::name()'],['../interface_v_m_state.html#a33f5b5455128cbddf8335bea41dcefaf',1,'VMState::name()'],['../interface_v_m_venue.html#a2f62a367952ca63ad28866e5b6355925',1,'VMVenue::name()']]],
  ['needsactivity',['needsActivity',['../interface_v_m_beacon.html#a47e9dfeb33ec881528003fe2ed9b5bc6',1,'VMBeacon']]],
  ['needsranging',['needsRanging',['../interface_v_m_beacon.html#ab3d69b60e16d7b18f1dbafbf0113a9b6',1,'VMBeacon']]],
  ['note',['note',['../interface_v_m_venue_open_hours.html#acb50e8fed4e1b349cfbadaba3cb8944c',1,'VMVenueOpenHours']]],
  ['notifyentrystateondisplay',['notifyEntryStateOnDisplay',['../interface_v_m_beacon.html#a42a8ad7afe3ac6db1f1f779349ff6a9e',1,'VMBeacon']]]
];
