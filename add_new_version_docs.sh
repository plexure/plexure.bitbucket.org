#!/bin/bash

# Get the platform and SDK version to add
PLATFORM=$1
SDK_VERSION=$2
DOCS_PATH=$3
VERSION_DIR_PATH="${PLATFORM}/${SDK_VERSION}"

# Get the path to copy the docs from, depending on the platform
if [ ${PLATFORM} == "android" ]
then
  platform_index_file="index.html"
else
    if [ ${PLATFORM} == "ios" ]
    then
      platform_index_file="classes.html"
    else
      echo 'Wrong platform sent as argument, use "android" or "ios" (without quotes)'
      # Exit with error
      exit 1
    fi
fi

# Create version dir to put the new docs in
mkdir ${VERSION_DIR_PATH}

# Copy the doc files from the source repository to a new dir in this one
printf "Copying new version docs from the source repo...\n"
cp -r "${DOCS_PATH}"/* ${VERSION_DIR_PATH}
rm -r ${PLATFORM}/latest/*
cp -r "${DOCS_PATH}"/* ${PLATFORM}/latest/

# Add new version to the index HTML
printf "\nAdding new version to the index file...\n"
perl -pi -e 's/<div id="intro"/<h3 class="doctext"><a href="'${SDK_VERSION}'\/'${platform_index_file}'">'${SDK_VERSION}'<\/a><\/h3>\n\t\t\t\t<div id="intro"/g' "${PLATFORM}/index.htm"

# We need to set the email and username or the push will fail
printf "\nPushing changes to Git...\n"
git config user.email "build@vmob.co"
git config user.name "PublishBot-v1.0.0"

git checkout master
git add -A .
git commit -am "[Plexure-SDK-Builder] Uploaded docs for Plexure ${PLATFORM} SDK version ${SDK_VERSION}"
git push origin master

printf '\nAll done    ʕ•ᴥ•ʔ \n'
